#!/bin/bash
echo "Starting RHEL install chef client"

curl -L https://www.opscode.com/chef/install.sh | sudo bash

mkdir /etc/chef
yum -y install wget
yum -y install https://packages.chef.io/stable/el/7/chef-12.14.89-1.el7.x86_64.rpm
wget  -O /home/ec2-user/key.pem "https://s3-us-west-2.amazonaws.com/se-media-share/anishavmvdc.pem"
chmod 400 /home/ec2-user/key.pem
scp -o StrictHostKeyChecking=no -i /home/ec2-user/key.pem ubuntu@54.244.188.255:/home/ubuntu/digitalocean-validator.pem /etc/chef/
echo "current_dir = File.dirname(__FILE__)" >> /etc/chef/client.rb
echo "log_level                :info" >> /etc/chef/client.rb
echo "log_location             STDOUT" >> /etc/chef/client.rb
echo "validation_client_name   'digitalocean-validator'" >> /etc/chef/client.rb
echo "validation_key           'etc/chef/digitalocean-validator.pem'" >> /etc/chef/client.rb
echo "chef_server_url          'https://ip-172-31-36-0.us-west-2.compute.internal/organizations/digitalocean'" >> /etc/chef/client.rb
echo "ssl_verify_mode :verify_none" >> /etc/chef/client.rb
/usr/bin/chef-client
 	
echo '{"run_list": ["role[role_linux_chefdeamon]"]}' >> /etc/chef/startup.json

/usr/bin/chef-client -j /etc/chef/startup.json
echo "Enabled chefclient to  ip-172-31-36-0.us-west-2.compute.internal successfuly\n"