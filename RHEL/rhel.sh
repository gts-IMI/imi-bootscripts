#!/bin/bash
echo "Starting RHEL install chef client"

yum -y install wget

yum -y install https://packages.chef.io/stable/el/7/chef-12.14.89-1.el7.x86_64.rpm


mkdir /etc/chef

wget  -O /etc/chef/digitalocean-validator.pem "https://s3-us-west-2.amazonaws.com/se-media-share/digitalocean-validator.pem"
wget  -O /etc/chef/client.rb "https://s3-us-west-2.amazonaws.com/se-media-share/client.rb"
/usr/bin/chef-client
 	
echo '{"run_list": ["role[role_linux_chefdeamon]"]}' >> /etc/chef/startup.json

/usr/bin/chef-client -j /etc/chef/startup.json
echo "Enabled chefclient to  ip-172-31-36-0.us-west-2.compute.internal successfuly\n"