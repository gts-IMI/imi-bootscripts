<script>
powershell -Command "(New-Object Net.WebClient).DownloadFile('https://s3-us-west-2.amazonaws.com/se-media-share/windows.ps1', 'C:\\windows.ps1')"

PowerShell -NoProfile -ExecutionPolicy Bypass -Command "& 'C:\\windows.ps1'" %

echo {"run_list": ["role[role_windows_chefdeamon]"]} >> C:\chef\startup.json

cd  C:\opscode\chef\bin\

chef-client -j C:\chef\startup.json
</script>
