
#########################################################################################################
#Function to Download Chef
#########################################################################################################

Add-Type -AssemblyName System.IO.Compression.FileSystem
function Unzip
{
    param([string]$zipfile, [string]$outpath)

    [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
}


function Download-ChefMSI{
New-Item -ItemType directory -Path C:\Chef
cd C:\Chef
$repo = "158.85.131.35"
$chef_server = "158.85.131.34" 
$msi = "chef-client-12.3.0-1.msi"

$chef_zip = "boots_nagios/chef.zip"
if (!(Test-Path c:\Chef\$msi)){
	Invoke-WebRequest -OutFile $msi http://$repo/$msi
}
else { 
	Write-host "Chef client file exists"
}

if(!(Test-path C:\Chef\chef.zip)){
	Invoke-WebRequest -OutFile chef.zip http://$repo/$chef_zip
}
else { Write-Host "Chef Folder already present"	
}

Unzip "C:\Chef\chef.zip" "C:\"
}


#########################################################################################################
#Function to Install Chef
#########################################################################################################

function Install-ChefMSI {
function Is-Installed( $program ) 
{ 
$x86 = ((Get-ChildItem "HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall") | Where-Object { $_.GetValue( "DisplayName" ) -like "*$program*" } ).Length -gt 0; 
$x64 = ((Get-ChildItem "HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall") | Where-Object { $_.GetValue( "DisplayName" ) -like "*$program*" } ).Length -gt 0; 
return $x86 -or $x64; 
}

if(Is-Installed("chef client")){
	Write-Host "Chef client already installed"
	[Environment]::SetEnvironmentVariable
     	( "Path", $env:Path, [System.EnvironmentVariableTarget]::Machine )

	[Environment]::SetEnvironmentVariable
     	( "INCLUDE", $env:INCLUDE, [System.EnvironmentVariableTarget]::User )

	[Environment]::SetEnvironmentVariable("Path", $env:Path + ";C:\opscode\chef\bin;C:\opscode\chef\embedded\bin", [EnvironmentVariableTarget]::Machine)

}
else {
	if (!(Test-Path C:\Chef\$msi)){
		throw "Path to the MSI File $(C:\Chef\$msi) is invalid. Please supply a valid MSI file"
 	}
 	$arguments = @(
        	"/i"
		"`"C:\Chef\$msi`""
 		"/qb"
        	"/norestart"
 	)
	if (!(Test-Path C:\Chef\)){
        	throw "Path to the Installation Directory $(C:\Chef\) is invalid. Please supply a valid installation directory"
    	}
    	$arguments += "INSTALLDIR=`"C:\Chef\`""
 	Write-Verbose "Installing C:\Chef\$msi....."

 	$process = Start-Process -FilePath msiexec  -ArgumentList $arguments -Wait -PassThru

 		[Environment]::SetEnvironmentVariable
    	 	( "Path", $env:Path, [System.EnvironmentVariableTarget]::Machine )

 		### Modify user environment variable ###
 		[Environment]::SetEnvironmentVariable
     		( "INCLUDE", $env:INCLUDE, [System.EnvironmentVariableTarget]::User )

		### from comments ###
		### Usage from comments - Add to the system environment variable ###
 		[Environment]::SetEnvironmentVariable("Path", $env:Path + ";C:\opscode\chef\bin;C:\opscode\chef\embedded\bin", [EnvironmentVariableTarget]::Machine)
		C:\opscode\chef\embedded\bin\gem install knife-windows
		Start-sleep -s 10
  }
}



$msi = "chef-client-12.3.0-1.msi" 
$ORDER_ID=Get-Content C:\Order_id.txt
$VMINDEX=Get-Content C:\Vm_index.txt
$ORDER=$ORDER_ID.Trim()
$VMIND=$VMINDEX.Trim()
Download-ChefMSI
Install-ChefMSI
