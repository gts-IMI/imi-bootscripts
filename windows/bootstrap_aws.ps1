function Get-ChefCert($ORDER,$VMIND){
        write-host "Adding the Chef Server to the hosts Files entry"
        ac -Encoding UTF8 C:\Windows\system32\drivers\etc\hosts "10.113.104.122 accochef.imi.ibm.com accochef"
        cd C:\Chef\
        $ip=get-WmiObject Win32_NetworkAdapterConfiguration|Where {$_.Ipaddress.length -gt 1}
        $IP1=$ip.ipaddress[0]
        $gw1=$ip.defaultipgateway[0]
        $IP2=$ip.ipaddress[2]
        $gw2=$ip.defaultipgateway[1]
        $ix1=$ip.index[0]
        $ix2=$ip.index[1]
        $name=hostname
        cd C:\Chef
        C:\opscode\chef\bin\knife ssl fetch
        Start-Sleep -s 20
        C:\opscode\chef\bin\knife ssl check
        Start-Sleep -s 10
        net start w32time
        cmd /c "netSh Advfirewall set allprofiles state off"
        C:\opscode\chef\embedded\bin\gem install knife-windows
        $pass1='QG2Dp7Be'
        write-host $pass1
        Start-Sleep -s 60
        C:\opscode\chef\bin\knife bootstrap windows winrm $IP1 -N $name -x Administrator -P $pass1 -r 'role[nagios_windows]'
}

$ORDER_ID=Get-Content C:\Order_id.txt
$VMINDEX=Get-Content C:\Vm_index.txt
$ORDER=$ORDER_ID.Trim()
$VMIND=$VMINDEX.Trim()
Get-ChefCert($ORDER,$VMIND)
