#!/bin/bash
echo "Starting ubuntu install chef client"

curl -L https://www.opscode.com/chef/install.sh | sudo bash
mkdir /etc/chef

wget  -O /etc/chef/digitalocean-validator.pem "https://s3-us-west-2.amazonaws.com/se-media-share/digitalocean-validator.pem"
wget  -O /etc/chef/client.rb "https://s3-us-west-2.amazonaws.com/se-media-share/client.rb"
/usr/bin/chef-client
 	
echo '{"run_list": ["role[role_ubuntu_chefdeamon]"]}' >> /etc/chef/startup.json

/usr/bin/chef-client -j /etc/chef/startup.json
echo "Enabled chefclient to  ip-172-31-36-0.us-west-2.compute.internal successfuly\n"